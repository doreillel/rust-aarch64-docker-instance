FROM arm64v8/debian
RUN apt update -y
RUN apt install curl -y
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
WORKDIR /app
# add libraries
# RUN apt install -y  libgtk-4-dev 
#RUN cargo build --release